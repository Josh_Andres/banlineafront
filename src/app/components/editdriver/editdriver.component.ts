import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DriversService } from '../../services/service.index';
import { Drivers} from '../../models/drivers.model';
import { NgForm } from '@angular/forms';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-editdriver',
  templateUrl: './editdriver.component.html',
  styleUrls: ['./editdriver.component.scss']
})
export class EditdriverComponent implements OnInit {
  public name: string;
  public apells: string;
  public doctype: string;
  public docnumber: string;
  public param: any;
  constructor(public route: ActivatedRoute,
    public driverService: DriversService,
    public snackBar: MatSnackBar
  ) {
    this.route.params.subscribe((params) => {
      this.param = params['id'];
    });
  }

  ngOnInit() {
    console.log(this.param);
     this.driverService.getDriver(this.param).subscribe((driver: any) => {
      console.log(driver);
      this.name = driver.driver.name;
      this.apells = driver.driver.apells;
      this.doctype = driver.driver.doctype;
      this.docnumber = driver.driver.docnumber;
     }, (error) => {
      console.log(error);
     });
  }

  updateDriver(d: NgForm) {
    const driv = new Drivers(
        d.value.name,
        d.value.apells,
        d.value.doctype,
        d.value.docnumber
    );
    this.driverService.updateDriver(driv, this.param).subscribe((response) => {
      console.log(response);
      this.snackBar.open('Datos actualizados correctamente', '', {
        duration: 2000
      });
    }, (error) => {
      this.snackBar.open('Error al intentar actualizar los datos', '', {
        duration: 2000
      });
      console.log(error);
    });
  }

}
