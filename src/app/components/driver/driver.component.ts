import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.scss']
})
export class DriverComponent implements OnInit {
  @Input() driver: any;
  @Input() driverUrl: any;
  constructor() { }

  ngOnInit() {
  }

}
