import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VehiclesService, ShippingService } from '../../services/service.index';
import { Vehicle} from '../../models/vehicle.model';
import { NgForm, FormControl, FormGroup} from '@angular/forms';
import {MatSnackBar} from '@angular/material';



@Component({
  selector: 'app-editvehicle',
  templateUrl: './editvehicle.component.html',
  styleUrls: ['./editvehicle.component.scss']
})
export class EditvehicleComponent implements OnInit {
  public param: any;
  public vehicleType: any;
  public plates: any;
  public dateSOAT: any;
  public shipping: any;
  public shippings: any = [];
  public status: any;
  public coordinates: string;
  type: FormControl;
  constructor(public route: ActivatedRoute,
    public vehicleService: VehiclesService,
    public shippingService: ShippingService,
    public snackBar: MatSnackBar
  ) {
    this.route.params.subscribe((params) => {
      this.param = params['id'];
    });
  }

  ngOnInit() {
    console.log(this.param);
    this.type = new FormControl('');
    this.getVehicle();
    this.getShippings();
  }

  getShippings() {
    this.shippingService.getShippings().subscribe((ships: any) => {
      console.log(ships);
      this.shippings = ships.shipping;
    }, (error) => {
      console.log(error);
    });
  }
/**
 * Update vehicle data
 * @param v : string
 */
  updateVehicle(v: NgForm) {
    const vehicle = new Vehicle(
      v.value.vehicleType,
      v.value.plates,
      v.value.dateSOAT,
      v.value.shipping,
      v.value.status = this.status,
      v.value.coordinates
    );
    console.log('vehile: --->', vehicle);
    this.vehicleService.updateVechicle(vehicle, this.param).subscribe((resp) => {
      console.log(resp);
      this.snackBar.open('Datos de vehiculo actualizados', '', {
        duration: 2000
      });
    }, (err) => {
      console.log(err);
    });
  }
/**
 * Get one vehicle data.
 */
  getVehicle() {
    this.vehicleService.getVehicle(this.param).subscribe((v: any) => {
      this.vehicleType = v.vehicle.vehicleType;
      this.plates = v.vehicle.plates;
      this.dateSOAT = v.vehicle.dateSOAT;
      this.shipping = v.vehicle.shipping;
      this.status = v.vehicle.status;
      this.coordinates = v.coordinates ? v.coordinates : ' ';
    }, (error) => {
      console.log(error);
    });
  }
}
