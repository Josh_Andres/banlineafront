import { Component, OnInit, Input } from '@angular/core';
import { Router,  NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss']
})
export class VehicleComponent implements OnInit {
  @Input() vehicle: any;
  public editar: boolean;

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  edit() {
    console.log('editar');
    return this.editar = true;
  }
  open() {
    const nav: NavigationExtras = {
      queryParams: { 'id':  this.vehicle._id},
      preserveFragment: true
    };
    // console.log(nav);
    this.router.navigate(['vehicle/edit'], nav);
  }
}
