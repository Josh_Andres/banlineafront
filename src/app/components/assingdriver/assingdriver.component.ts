import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { DriversService, VehiclesService } from '../../services/service.index';
import { Assing } from './../../models/assing.model';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-assingdriver',
  templateUrl: './assingdriver.component.html',
  styleUrls: ['./assingdriver.component.scss']
})
export class AssingdriverComponent implements OnInit {
  public param: any;
  public drivers: any = [];
  public driver: any;
  public coordinates: string;

  constructor(
    public route: ActivatedRoute,
    public driverService: DriversService,
    public vehicleService: VehiclesService,
    public snackBar: MatSnackBar
  ) {
    this.route.params.subscribe((params) => {
      this.param = params['id'];
    });
  }

  ngOnInit() {
    this.getDrivers();
    this.getVehicle();
  }

  assing(as: NgForm) {
    const assig =  new Assing(
          as.value.driver
    );
    console.log(assig);
    if (as.value.driver) {
      this.vehicleService.assingVehicle(assig, this.param).subscribe((updated) => {
        console.log(updated);
        this.snackBar.open(updated.message, '', {
          duration: 2000
         });
      }, (error) => {
        console.log(error);
        this.snackBar.open(error.error.message, '', {
          duration: 2000
         });
      });
    } else {
      this.snackBar.open('Campos Incompletos', '', {
        duration: 2000
       });
    }

  }

  getDrivers() {
    this.driverService.getDriversList().subscribe((drivers: any) => {
      console.log(drivers);
       this.drivers = drivers.drivers;
    }, (error) => {
      console.log(error);
    });
  }

  getVehicle() {
    this.vehicleService.getVehicle(this.param).subscribe((vehicle: any) => {
      console.log(vehicle.vehicle.coordinates);
      this.coordinates = `https://maps.googleapis.com/maps/api/staticmap?autoscale=1&size=300x200&maptype=roadmap&format=png&visual_
      refresh=true&markers=size:small%7Ccolor:0xff7d18%7Clabel:1%7C
      ${vehicle.vehicle.coordinates}
      &key=AIzaSyAJYOLb-19lRa3SY4oNCmynR6IqNiiLZuA`;
    }, (error) => {
      console.log(error);
    });
  }
}
