export class DriversCreate {
  constructor(
    public name: string,
    public apells: string,
    public doctype: string,
    public docnumber: string,
    public status: string,
    public vehicle: string,
  ) {}
}
