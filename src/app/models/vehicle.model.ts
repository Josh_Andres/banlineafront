export class Vehicle {
  constructor(
    public vehicleType: string,
    public plates: string,
    public dateSOAT: string,
    public shipping: string,
    public status: string,
    public coordinates: string
  ) {}
}
