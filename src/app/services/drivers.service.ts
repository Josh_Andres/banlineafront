import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { URL_SERVICE } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class DriversService {

  constructor(public _http: HttpClient) { }

  createDriver(drivers: any) {
    return this._http.post(URL_SERVICE + 'drivers/', drivers).pipe(map((driver) => {
      return driver;
    }, (error) => {
      return error;
    }));
  }

  updateDriver(driver: any, id: any) {
    return this._http.put(URL_SERVICE + `drivers/${id}`, driver).pipe(map((driverupdate) => {
      return driverupdate;
    }, (error) => {
      return error;
    }));
  }

  getDrivers() {
    return this._http.get(URL_SERVICE + 'drivers/all', ).pipe(map((drivers) => {
      return drivers;
    }, (error) => {
      return error;
    }));
  }

  getDriversList() {
    return this._http.get(URL_SERVICE + 'drivers/all?list=true', ).pipe(map((drivers) => {
      return drivers;
    }, (error) => {
      return error;
    }));
  }
  
  getDriver(id: any) {
    return this._http.get(URL_SERVICE + `drivers/${id}`, ).pipe(map((driver) => {
      return driver;
    }, (error) => {
      return error;
    }));
  }
}
