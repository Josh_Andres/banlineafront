import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { URL_SERVICE } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class ShippingService {

  constructor(public _http: HttpClient) { }

  getShippings() {
    return this._http.get(URL_SERVICE + 'shipping/all').pipe(map((shippings) => {
        return shippings;
    }, (error) => {
        return error;
    }));
  }
  getShipping() {

  }

  createShipping(shipping: any) {
    return this._http.post(URL_SERVICE + 'shipping/', shipping).pipe(map((ship) => {
      return ship;
    }, (error) => {
      return error;
    }));
  }

  updateShipping() {

  }
}
