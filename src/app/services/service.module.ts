import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import {VehiclesService, DriversService, ShippingService} from './service.index';


@NgModule({
  imports: [
    HttpClientModule,
    CommonModule
  ],
  declarations: [],
  providers: [VehiclesService, DriversService, ShippingService]
})
export class ServiceModule { }
