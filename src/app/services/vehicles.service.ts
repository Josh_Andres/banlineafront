import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { URL_SERVICE } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  constructor(public _http: HttpClient) { }

  createVechicle(vehicledata: any) {
    return this._http.post(URL_SERVICE + 'vehicles/', vehicledata).pipe(map((vehicle: any) => {
      return vehicle;
    }, (error) => {
      console.log('errService', error);
      return error;
    }));
  }
  getVecicles() {
    console.log(URL_SERVICE + 'vehicles/all');
    return this._http.get(URL_SERVICE + 'vehicles/all').pipe(map((vehicles: any) => {
      return vehicles;
    }, (error) => {
      return error;
    }));
  }
  getVehicle(id: any) {
    return this._http.get(URL_SERVICE + `vehicles/${id}`).pipe(map((vehi) => {
      return vehi;
    }, (err) => {
      return err;
    }));
  }

  updateVechicle(vehicledata: any, id: any) {
    console.log(URL_SERVICE + `vehicles/${id}`);
    return this._http.put(URL_SERVICE + `vehicles/${id}`, vehicledata).pipe(map((vehicle: any) => {
      console.log(vehicle);
      return vehicle;
    }, (error) => {
      console.log('errService', error);
      return error;
    }));
  }

  assingVehicle(vehicleData: any, id: any) {
    return this._http.put(URL_SERVICE + `vehicles/assigned/${id}`, vehicleData).pipe(map((vehicle: any) => {
      console.log(vehicle);
      return vehicle;
    }, (error) => {
      console.log('errService', error);
      return error;
    }));
  }

}
