import { Component, OnInit } from '@angular/core';
import { VehiclesService } from '../../services/service.index';

@Component({
  selector: 'app-vehiclelist',
  templateUrl: './vehiclelist.component.html',
  styleUrls: ['./vehiclelist.component.scss']
})
export class VehiclelistComponent implements OnInit {
  public vehicles: any = [];
  constructor(public vehicleService: VehiclesService) { }

  ngOnInit() {
    this.vehicleService.getVecicles().subscribe((vehicles: any) => {
      console.log(vehicles);
      this.vehicles = vehicles.vehicles;
    }, (error) => {
      console.log(error);
      return error;
    });
  }

}
