import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './../material';
// modulo search
import { SharedModule } from '../shared/shared.module';
// pages routes
import { PAGES_ROUTES } from './pages.routes';
import { PagesComponent } from './pages.component';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../pipes/pipes.module';
import { DriversComponent } from './drivers/drivers.component';
import { VehicleComponent} from '../components/vehicle/vehicle.component';

import { ShippingComponent } from '../components/shipping/shipping.component';
import { ShippingadminComponent } from './shippingadmin/shippingadmin.component';
import { VehiclescreateComponent } from './vehiclescreate/vehiclescreate.component';
import { DrivercreateComponent } from './drivercreate/drivercreate.component';
import { VehiclelistComponent } from './vehiclelist/vehiclelist.component';
import { EditvehicleComponent } from '../components/editvehicle/editvehicle.component';
import { DriverComponent } from '../components/driver/driver.component';
import { DriverlistComponent } from './driverlist/driverlist.component';
import { EditdriverComponent } from '../components/editdriver/editdriver.component';
import { AssingdriverComponent } from '../components/assingdriver/assingdriver.component';




// servicios
// import { ServiceModule } from '../services/service.module';

// temporal
@NgModule({
  declarations: [
  PagesComponent,
  VehicleComponent,
  DriversComponent,
  ShippingComponent,
  ShippingadminComponent,
  VehiclescreateComponent,
  DrivercreateComponent,
  VehiclelistComponent,
  EditvehicleComponent,
  DriverComponent,
  DriverlistComponent,
  EditdriverComponent,
  AssingdriverComponent
  ],
  exports: [
    DriversComponent,
    VehicleComponent,
    ShippingComponent,
    ShippingadminComponent,
    VehiclescreateComponent,
    EditvehicleComponent,
    DriverComponent,
    EditdriverComponent,
    AssingdriverComponent
  ],
  imports: [
    MaterialModule,
    PipesModule,
    CommonModule,
    SharedModule,
    PAGES_ROUTES,
    FormsModule,
    FlexLayoutModule
  ]
})

export class PagesModule {}
