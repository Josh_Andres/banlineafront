import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Router} from '@angular/router';
import { VehiclesService, ShippingService } from '../../services/service.index';
import {Vehicle} from './../../models/vehicle.model';
import {MatSnackBar} from '@angular/material';


@Component({
  selector: 'app-vehiclescreate',
  templateUrl: './vehiclescreate.component.html',
  styleUrls: ['./vehiclescreate.component.scss']
})
export class VehiclescreateComponent implements OnInit {
  public vehicleType: string;
  public plates: string;
  public dateSOAT: string;
  public shipping: string;
  public status: string;
  public cordDestino: string;

  public shippings: any = [];

  constructor(
    public _vehicleService: VehiclesService,
    public _shipService: ShippingService,
    public router: Router,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this._shipService.getShippings().subscribe((shippings: any) => {
      console.log('shippings', shippings);
        this.shippings = shippings.shipping;
    }, (error) => {
      console.log(error);
    });
  }
  postVehicle(forma: NgForm) {
    if (!forma.value.status) {
      forma.value.status = 'active';
    }
    const vehicle = new Vehicle(
      forma.value.vehicleType,
      forma.value.plates,
      forma.value.dateSOAT,
      forma.value.shipping,
      forma.value.status,
      forma.value.cordDestino
    );
    console.log(forma);
    this._vehicleService.createVechicle(vehicle).subscribe((vehi) => {
      console.log(vehi);
      this.snackBar.open(vehi.message, '', {duration: 2000});
      this.router.navigate(['shipping']);
      // return vehi;
    }, (error) => {
      this.snackBar.open(error.error.message, '', {duration: 2000});
      console.log(error);
      // return error;
    });
  }
}
