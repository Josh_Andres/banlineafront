import { Component, OnInit } from '@angular/core';
import { ShippingService } from '../../services/service.index';


@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss']
})
export class DriversComponent implements OnInit {
public shipping: any = [];
  constructor(public shippingService: ShippingService) { }

  ngOnInit() {
    this.shippingService.getShippings().subscribe((shippings: any) => {
      console.log(shippings);
      this.shipping = shippings.shipping;
    }, (error) => {
      console.log(error);
      return error;
    });
  }

}
