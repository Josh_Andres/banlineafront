import { Component, OnInit } from '@angular/core';
import { DriversService } from '../../services/service.index';

@Component({
  selector: 'app-driverlist',
  templateUrl: './driverlist.component.html',
  styleUrls: ['./driverlist.component.scss']
})
export class DriverlistComponent implements OnInit {
public drivers: any = [];
  constructor(public driverService: DriversService) { }

  ngOnInit() {
    this.getDrivers();
  }

  getDrivers() {
    this.driverService.getDrivers().subscribe((drivers: any) => {
      console.log(drivers);
       this.drivers = drivers.drivers;
    }, (error) => {
      console.log(error);
    });
  }
}
