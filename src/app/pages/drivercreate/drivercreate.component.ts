import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { DriversService } from '../../services/service.index';
import { DriversCreate } from '../../models/driverscreate.model';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-drivercreate',
  templateUrl: './drivercreate.component.html',
  styleUrls: ['./drivercreate.component.scss']
})
export class DrivercreateComponent implements OnInit {
  public name: String;
  public apells: String;
  public doctype: String;
  public docnumber: String;
  public status: String;
  public vehicle: String;

  constructor(
    public _driverService: DriversService,
    public router: Router,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }
  postDriver(driv: NgForm) {
    console.log(driv.value);
    if (!driv.value.status) {
      driv.value.status = 'active';
    }
    if (!driv.value.status) {
      driv.value.vehicle = 'unassigned';
    }
    const driver = new DriversCreate(
      driv.value.name,
      driv.value.apells,
      driv.value.doctype,
      driv.value.docnumber,
      driv.value.status,
      driv.value.vahicle
    );
    this._driverService.createDriver(driver).subscribe((response: any) => {
      this.snackBar.open(response.message, '', {
        duration: 2000
      });
      this.router.navigate(['drivers']);
    }, (error: any) => {
      this.snackBar.open(error.error.message, '', {duration: 2000});
      console.log(error);
    });
  }
}
