import { RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages.component';
import { DriversComponent } from './drivers/drivers.component';
import { ShippingadminComponent } from './shippingadmin/shippingadmin.component';
import { VehiclescreateComponent } from './vehiclescreate/vehiclescreate.component';
import { DrivercreateComponent } from './drivercreate/drivercreate.component';
import { VehiclelistComponent } from './vehiclelist/vehiclelist.component';
import { EditvehicleComponent } from '../components/editvehicle/editvehicle.component';
import { DriverlistComponent } from './driverlist/driverlist.component';
import { EditdriverComponent } from '../components/editdriver/editdriver.component';
import { AssingdriverComponent } from '../components/assingdriver/assingdriver.component';





const pagesRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    canActivate: [],
    children: [
      {path: 'shipping', component: VehiclelistComponent , data: { titulo: 'Shipping'}},
      {path: 'drivers', component: DriverlistComponent , data: { titulo: 'Drivers'}},
      {path: 'drivers/create', component: DrivercreateComponent , data: { titulo: 'Drivers'}},
      {path: 'vehicles/create', component: VehiclescreateComponent , data: { titulo: 'Vehicles'}},
      {path: 'shipping/create', component: ShippingadminComponent , data: { titulo: 'Shipping'}},
      {path: 'shipping/vehicle/edit/:id', component: EditvehicleComponent , data: { titulo: 'Vehicle Edit'}},
      {path: 'drivers/driver/edit/:id', component: EditdriverComponent , data: { titulo: 'Driver Edit'}},
      {path: 'shipping/vehicle/assing/:id', component: AssingdriverComponent , data: { titulo: 'Driver Assing'}},
      {path: '', redirectTo: 'shipping', pathMatch: 'full'}
    ]
  }
];

export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );
