import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ShippingService } from '../../services/service.index';
import {Shipping} from '../../models/shipping.model';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-shippingadmin',
  templateUrl: './shippingadmin.component.html',
  styleUrls: ['./shippingadmin.component.scss']
})
export class ShippingadminComponent implements OnInit {
  public name: String;
  public description: String;

  constructor(
    public shippingService: ShippingService,
    public router: Router,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  postShipping(forma: NgForm) {
    const shipping = new Shipping(forma.value.name, forma.value.description);
    this.shippingService.createShipping(shipping).subscribe((ship) => {
      console.log('flota creada correctamente');
      this.snackBar.open('Flota creada correctamente', '', {
        duration: 2000
      });
      this.router.navigate(['shipping']);
    }, (error) => {
      this.snackBar.open(error.error.message, '', {
        duration: 2000
      });
      console.log('error al intentar crear shipping', error);
    });
  }
}
