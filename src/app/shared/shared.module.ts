import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NopagefoundComponent } from './nopagefound/nopagefound.component';
import { HeaderComponent } from './header/header.component';
import { PipesModule } from '../pipes/pipes.module';
import { MaterialModule } from '../material';




@NgModule({
  imports: [
    PipesModule,
    CommonModule,
    RouterModule,
    MaterialModule
  ],
  declarations: [
    NopagefoundComponent,
    HeaderComponent
  ],
  exports: [
    NopagefoundComponent,
    HeaderComponent
  ]
})
export class SharedModule { }
