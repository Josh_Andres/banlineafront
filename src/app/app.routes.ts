import { RouterModule, Routes } from '@angular/router';
import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';
import { DriversComponent } from './pages/drivers/drivers.component';


const appRoutes: Routes = [
  // { path: '',   redirectTo: '/heroes', pathMatch: 'full' }
  {path: 'drivers', component: DriversComponent},
  {path: '**', component: NopagefoundComponent}
];

export const APP_ROUTES = RouterModule.forRoot( appRoutes, {useHash: true});
