'use strict'

const express = require ('express')
const app = express()
const path = require('path')

app.use(express.static(__dirname + '/dist/banLinea'))
console.log(__dirname+'/dist');
app.listen(process.env.PORT || 1000);

app.get('/*',function(req, res){
	console.log(path.join(__dirname + '/dist/banLinea/index.html'))
    res.sendFile(path.join(__dirname + '/dist/banLinea/index.html'))
})

console.log('Listening');